#!/usr/bin/env ruby
# encoding: utf-8
require 'bunny'
require 'time'

require ::File.expand_path('../config/application',  __FILE__)

#connection = Bunny.new ("amqp://#{ENV['AMQP_USERNAME']s:%<PASSWORD>s@%<HOST>s:%<PORT>s" % {}
connection = Bunny.new "amqp://#{ENV['AMQP_USERNAME']}:#{ENV['AMQP_PASSWORD']}@#{ENV['AMQP_HOST']}:#{ENV['AMQP_PORT']}"
connection.start
channel = connection.create_channel
exchange  = channel.topic "sync"#, :auto_delete => true

begin
  puts " [*] Waiting for messages. To exit press CTRL+C"
  channel.queue("rails").bind(exchange, :routing_key => "rails.*").subscribe(:block => true) do |delivery_info, properties, body|
    t = Time.now
    hhmmss = "#{t.hour}:#{t.min}:#{t.sec}"
    puts " [x] #{delivery_info.routing_key} - Received at #{hhmmss} #{body}"
    collection = delivery_info.routing_key.split('.')[1]
    mongoidsync = MongoidSync.new
    mongoidsync.write(body,collection)
  end
  # channel.queue("rails.agenda").bind(exchange, :routing_key => "rails.agenda").subscribe(:block => true) do |delivery_info, properties, body|
  #   puts " [x] rails.agenda - Received #{body}"
  # end
rescue Interrupt => _
  connection.close
  exit(0)
end

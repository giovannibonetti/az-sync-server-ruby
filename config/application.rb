#Disable stdout buffer -> print immediately to stdout (slow, but useful for development)
$stdout.sync = true

require 'rubygems'

# Set up gems listed in the Gemfile.
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)
require 'bundler/setup' if File.exists?(ENV['BUNDLE_GEMFILE'])

#require "bunny"
#%w(mongoid json uuid date time).each {|package| require package}
require 'mongoid'
#%w(date time).each {|package| require package}

#Mongoid.load!("./config/mongoid.yml", :production)
Mongoid.load!(File.expand_path('../mongoid.yml', __FILE__), :production)

#Dir["./initializers/*.rb"].each {|file| require file }
Dir[File.expand_path('../initializers/*.rb', __FILE__)].each {|file| require file }

## App folders - Models and controllers
#require "./models/agenda"  
#Dir["./models/*.rb"].each {|file| require file }
Dir[File.expand_path('../../app/models/*.rb', __FILE__)].each {|file| require file }

Dir[File.expand_path('../../app/controllers/*.rb', __FILE__)].each {|file| require file }
#require "./mongoid_sync"

ActiveSupport::Inflector.inflections do |inflect|
  inflect.irregular "agenda", "agenda"
  inflect.irregular "agenda_sala", "agenda_salas"
  inflect.irregular "cliente", "clientes"
  inflect.irregular "customer", "customers"
  inflect.irregular "habilidade", "habilidades"
  inflect.irregular "habilidade_rateio", "habilidades_rateio"
  inflect.irregular "profissional", "profissionais"
  inflect.irregular "profissional_assistente", "profissionais_assistentes"
  inflect.irregular "profissional_habilidade", "profissionais_habilidades"
  inflect.irregular "profissional_horario", "profissionais_horarios"
  inflect.irregular "profissional_produtividade", "profissionais_produtividade"
  inflect.irregular "servico", "servicos"
end
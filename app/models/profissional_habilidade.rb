# encoding: utf-8
class ProfissionalHabilidade
  include Mongoid::Document

  field :customer_id, default: 1
  field :CODIGO, default: 1
  field :CODIGO_SERVICO, default: 1
  field :TITULAR, default: "Não"
  field :TITULAR_USAR_TABELA, default: "Não"
  field :ASSISTENTE, default: "Sim"
  field :ASSISTENTE_RATEIO, type: Integer#, default: 10
  field :ASSISTENTE_RATEIO_TITULAR, type: Integer#, default: 0
  field :ASSISTENTE_RATEIO_CASA, type: Integer#, default: 10
  field :PAGO_POR_PORCENTAGEM, default: "Não"

end
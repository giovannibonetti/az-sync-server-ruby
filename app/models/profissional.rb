# encoding: utf-8
class Profissional
  require 'date'
  include Mongoid::Document

  field :customer_id, default: 1
  field :CODIGO, default: 1
  field :CODIGO_DE_BARRAS, default: "00000001"
  field :LIXO, default: "F"
  field :APELIDO, default: "Deulzir"
  field :DATA_DE_CADASTRO, default: Date.parse("2002-09-11T14:04:56Z")
  field :ULTIMA_EDICAO, default: Date.parse("2012-10-23T10:13:21Z")
  field :NOME, default: "Deulzir Bedin"
  field :FOTOGRAFIA, default: nil
  field :ENDERECO, default: "Av. Presidente Kennedy, 245 F. 57"
  field :BAIRRO, default: "Campinas"
  field :CEP, default: "88101-001"
  field :CIDADE, default: "São José"
  field :UF, default: "SC"
  field :EMAIL, default: "deulzir@bol.com.br"
  field :NASCIMENTO, default: Date.parse("1960-09-11T00:00:00Z")
  field :SEXO, default: "Feminino"
  field :ESTADO_CIVIL, default: "Divorciado(a)"
  field :TELEFONES, default: "Casa - (48) 241-1572\r\nCelular - (48) 9972-0605\r\n"
  field :CPF, default: "034.577.979-78"
  field :ATIVO, default: "Não"
  field :GRUPO_RATEIO, default: "Quinzenal"
  field :FUNCAO, default: "Cabeleireiro"
  field :UTILIZA_AGENDA, default: "Sim"
  field :RATEIO_DIFERENCIADO_PRODUTOS, default: "Não"
  field :DESCONTA_CUSTO_PRODUTOS, default: "ANT"
  field :CONTA_BANCO, default: "Bradesco"
  field :MOSTRAR_AVISOS, default: "Não"
  field :LIMITES_ENCAIXE_AGENDA, default: 1
  field :DESCONTA_TAXA_ADMINISTRATIVA, default: "Sim"
  field :PERMITE_ACESSO_MOD_PROFISSIONAL, default: "Sim"
  field :VALOR_LIMITE_VALE, default: 200
  field :UTILIZA_RODIZIO, default: "Sim"
  field :CUSTO_ANTECIPACAO_RATEIO, default: 0
  field :ADIANTAMENTO_RATEIO_VENDA_PACOT, default: "Não"
  field :APLICA_ADIANT_RATEIO_APOS_CALC, default: "Não"

end
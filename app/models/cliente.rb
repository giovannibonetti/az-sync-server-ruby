# encoding: utf-8
class Cliente
  include Mongoid::Document

    field :customer_id, type: Integer
    field :CODIGO, type: Integer
    field :CODIGO_DE_BARRAS, default: "00000001"
    field :ATIVO, default: "Sim"
    field :LIXO, default: "F"
    field :ULTIMA_EDICAO, type: DateTime
    field :NOME
    field :ENDERECO
    field :BAIRRO
    field :CEP
    field :CIDADE
    field :UF
    field :TELEFONES, default: "Casa - (41) 1234-5678"
    field :NASCIMENTO, type: DateTime
    field :EMAIL
    field :SEXO#, default: "Masculino"
    field :ESTADO_CIVIL#, default: "Solteiro(a)"
    field :RG, default: "500"
    field :NATURALIDADE
    field :PERFIL
    field :VEICULO
    field :INDICACAO
    field :PREFERENCIA, type: Integer, default: 1
    field :MALA_DIRETA, default: "Sim"
    field :ULTIMA_VISITA, type: DateTime
    field :DATA_DE_CADASTRO, type: DateTime
    field :FOTO_PADRAO, type: Integer, default: 1
    field :ESTADO_DO_CABELO
    field :TIPO_DE_CABELO
    field :COMPRIMENTO_DO_CABELO
    field :NATUREZA_DO_CABELO
    field :TOTAL_DE_VISITAS, type: Integer, default: 0
    field :FICHA_TECNICA
    field :ANAMNESE
    field :ULTIMO_SYNC, default: 396
    field :DATA_ULTIMO_SYNC, type: DateTime
    field :TIPO_SYNC, default: "A"
    field :CREDITO_CLIENTE, type: Float
    field :MOSTRAR_AVISOS, default: "Não"
    field :PONTOS, type: Integer, default: 0
    field :LIBERA_LIVRO_SIM_NAO, default: "sim"
    field :CODIGO_ULTIMO_USUARIO, type: Integer, default: 0
    field :RECEBER_SMS, default: "Sim"

end
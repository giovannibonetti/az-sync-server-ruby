# encoding: utf-8
class ProfissionalProdutividade
  include Mongoid::Document

  field :customer_id, type: Integer
  field :PROFISSIONAL, type: Integer
  field :SERVICO, type: Integer
  field :DURACAO, type: Integer

end
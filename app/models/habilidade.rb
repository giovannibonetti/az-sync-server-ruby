# encoding: utf-8
class Habilidade
  include Mongoid::Document

  field :customer_id, type: Integer
  field :CODIGO, type: Integer
  field :NOME
  field :DESCRICAO

end
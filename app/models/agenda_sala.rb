# encoding: utf-8
class AgendaSala
  include Mongoid::Document

  field :customer_id, type: Integer
  field :CODIGO, type: Integer
  field :NOME
  field :COR, type: Integer, default: 16746751

end
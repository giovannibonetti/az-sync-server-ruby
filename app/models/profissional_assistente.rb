# encoding: utf-8
class ProfissionalAssistente
  include Mongoid::Document

  field :customer_id, type: Integer
  field :CODIGO_TITULAR, type: Integer
  field :CODIGO_ASSISTENTE, type: Integer
  field :NOME

end
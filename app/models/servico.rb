# encoding: utf-8
class Servico
  include Mongoid::Document

  field :customer_id, type: Integer
  field :CODIGO, type: Integer
  field :LIXO, default: "F"
  field :ULTIMA_EDICAO, type: DateTime
  field :DATA_DE_CADASTRO, type: DateTime
  field :NOME, default: "Teste Rangel 1"
  field :DESCRICAO, default: "teste rangel 1"
  field :RATEIO, default: 15
  field :ATIVO, default: "Sim"
  field :ACEITA_DESCONTO, default: "Sim"
  field :ACEITA_AJUSTE, default: "Sim"
  field :PRECO_DE_VENDA, default: 15
  field :CUSTO_COM_PRODUTOS, type: Integer, default: 1
  field :DESCONTA_CUSTO_RATEIO, default: "Sim"
  field :PONTOS, type: Integer, default: 20
  field :HABILITA_DESCONTO_PROGRAMADO, default: "Não"
  field :DESCONTO_EM_PERCENTUAL, default: "Não"
  field :DESCONTO_DOM_PERC, type: Float, default: 0
  field :DESCONTO_SEG_PERC, type: Float, default: 0
  field :DESCONTO_TER_PERC, type: Float, default: 0
  field :DESCONTO_QUA_PERC, type: Float, default: 0
  field :DESCONTO_QUI_PERC, type: Float, default: 0
  field :DESCONTO_SEX_PERC, type: Float, default: 0
  field :DESCONTO_SAB_PERC, type: Float, default: 0
  field :DESCONTO_DOM_REAIS, type: Float, default: 0
  field :DESCONTO_SEG_REAIS, type: Float, default: 0
  field :DESCONTO_TER_REAIS, type: Float, default: 0
  field :DESCONTO_QUA_REAIS, type: Float, default: 0
  field :DESCONTO_QUI_REAIS, type: Float, default: 0
  field :DESCONTO_SEX_REAIS, type: Float, default: 0
  field :DESCONTO_SAB_REAIS, type: Float, default: 0
  field :FAMILIA#, default: "cabeleireiros"
  field :DESCONTA_OPERACIONAL_RATEIO, default: "Não"
  field :CUSTO_OPERACIONAL, type: Integer, default: 0
  field :OPERACIONAL_ARREDONDAMENTO, default: "Não"
  field :RATEIO_POR_PORCENTAGEM, default: "Não"
  field :LIMITES_EMCAIXE_AGENDA, type: Integer, default: 0
  field :ATUALIZAR_AUTO_CUSTO_PRODUTOS, default: "Não"
  field :CODIGO_USUARIO_ULT_EDICAO, type: Integer, default: 0
  field :AZ_CLIENTE_MOSTRAR_TABELA, default: "Sim"
  field :SOMAR_DURACAO_VINCULADOS, default: "Não"
  field :AZ_CLIENTE_FLAG_TBPRECO, default: "Não"

end
# encoding: utf-8
class ProfissionalHorario
  include Mongoid::Document

  field :customer_id, type: Integer
  field :CODIGO, type: Integer
  field :DIA, type: Integer
  field :INICIO, type: DateTime
  field :FIM, type: DateTime

end
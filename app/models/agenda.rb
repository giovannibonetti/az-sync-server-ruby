# encoding: utf-8
class Agenda
  include Mongoid::Document

  field :customer_id, type: Integer

  field :CODIGO_PROFISSIONAL, type: Integer
  field :CODIGO_CLIENTE, type: Integer
  field :CODIGO_SERVICO, type: Integer
  field :CODIGO_SALA, type: Integer, default: 0

  field :DBKEY
  field :ID_AGENDAMENTO, type: Integer, default: 9999

  field :DATA_OPERACAO, type: DateTime
  field :DATA_AGENDAMENTO, type: DateTime
  field :HORA_INICIAL, type: DateTime
  field :HORA_FINAL, type: DateTime
  field :DURACAO, type: Integer
  field :AGENDAMENTO, default: "Horário: INICIO - FIM\r\nNOME DO SERVICO"

  #Default fields
  field :TIPO_AGENDAMENTO, default: "Agendamento"
  field :COR, type: Integer, default: 10350847
  field :USUARIO, default: "SUPERVISOR"
  field :CODIGO_USUARIO, type: Integer, default: 0
  field :ATENDIMENTO_EFETUADO, default: "Não"
  field :CONFIRMADO, default: "Não"
end
# encoding: utf-8
class Customer
  include Mongoid::Document

  field :customer_id, default: 1
  field :customer_name, default: "Salon Giovanni"
  field :customer_jndi, default: "salon_giovanni"
  field :active, default: 1

end
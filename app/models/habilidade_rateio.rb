# encoding: utf-8
class HabilidadeRateio
  include Mongoid::Document

  field :customer_id, type: Integer
  field :CODIGO, type: Integer
  field :CODIGO_SERVICO, type: Integer
  field :TITULAR, default: "Sim"
  field :TITULAR_USAR_TABELA, default: "Não"
  field :TITULAR_RATEIO, type: Integer#, default: 70
  field :ASSISTENTE, default: "Não"
  field :PAGO_POR_PORCENTAGEM, default: "Sim"

end
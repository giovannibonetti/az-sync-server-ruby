# encoding: utf-8
class MongoidSync
  require 'json'
  require 'uuid'
  require 'date'

  def initialize(*args)    
  end

  def write(json, collection)
    fields = JSON.parse(json)
    puts "- JSON parsed"

    case collection
    when 'agenda'
      #agendamento = Agenda.create!(
      agendamento = Agenda.new(
        "DBKEY" => UUID.new.generate,
        "customer_id" => 5,
        "DATA_OPERACAO" => DateTime.now,
        "DATA_AGENDAMENTO" => DateTime.parse("#{fields['scheduling_date']} 00:00 UTC"),
        "HORA_INICIAL" => DateTime.parse("01/01/1970 #{fields['start_time']} UTC"),
        "HORA_FINAL" => DateTime.parse("01/01/1970 #{fields['end_time']} UTC"),
        "DURACAO" => 30,
        "TIPO_AGENDAMENTO" => "Agendamento",
        "CODIGO_PROFISSIONAL" => 90,
        "CODIGO_CLIENTE" => 4817,
        "CODIGO_SERVICO" => 133,
        "CODIGO_SALA" => 0,
      )
      puts "- agendamento efetuado no MongoDB: " + agendamento.to_json
      agendamento.save
    # when 'cliente'
    #   defaults = {


    #   }
    #   record = defaults.merge(record)
    # end
    else
      puts "- Tabela desconhecida!"
    end

  end

#   def DateTimefrom(dt,tm,utc='-3')
#     dt = Date.parse(dt) if dt.is_a?(String)
#     tm = Time.zone.parse(tm) if tm.is_a?(String)
#     ary = [dt.year, dt.month, dt.day, tm.hour, tm.min, tm.sec].map(&:to_i)
#     DateTime.new(*ary, utc)
#   end
# end

end
